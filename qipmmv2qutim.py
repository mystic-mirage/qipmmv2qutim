#!/usr/bin/env python

import sys
import os
import re
from datetime import datetime
import cgi
import json

def writeQutIM(out, hi, u, d):
    u = u.replace('@', '%0040')
    if not os.path.isdir(out):
        os.mkdir(out)
    fn = '%s.%s.json' % (u, d)
    f = open(out + fn, 'w+')
    f.write(json.dumps(hi, indent = 1, ensure_ascii = False))
    f.close()
    print ' - Writing file: %s' % fn

def qip2qutim(filename, output):
    result = [False, 0]

    f = open(filename)
    ptext = f.read()
    f.close()

    uin = os.path.splitext(os.path.split(filename)[-1])[0]

    text = ptext.decode('cp1251', 'replace')
    text = text.encode('utf8')

    text = text.replace('\r', '')

    t = text.split('-' * 38)

    h = []
    l_dt = ''

    for l in t:
        m = l.split('\n')
        if m[0] in ('>-', '*-'):
            h_in = False
        elif m[0] == '<-':
            h_in = True
        else:
            continue
        h_dt = datetime.strptime(m[1].split('(')[-1], '%H:%M:%S %d/%m/%Y)')
        t_dt = h_dt.strftime('%Y-%m-%dT%H:%M:%S')
        dt = h_dt.strftime('%Y%m')
        if not l_dt: l_dt = dt
        h_tx = '\n'.join(m[2:-2])
        if dt != l_dt:
            writeQutIM(output, h, uin, l_dt)
            result = [True, result[1] + 1]
            h = []
            l_dt = dt

        h.append({'subject': '', 'html': cgi.escape(h_tx), 'datetime': t_dt, 'in': h_in, 'text': h_tx})
    else:
        if h:
            writeQutIM(output, h, uin, l_dt)
            result = [True, result[1] + 1]
    return result

def main():
    sep = os.path.sep
    rz = zz = er = sk = 0

    try:
        i = sys.argv[1]
    except:
        i = '.'
    finally:
        ipath = (i + sep).replace(sep * 2, sep)

    try:
        o = sys.argv[2]
    except:
        o = i + 'qutim-history'
    finally:
        opath = (o + sep).replace(sep * 2, sep)

    for fl in [f for f in os.listdir(ipath) if re.match(r'^(?!_srvlog).*\.txt', f)]:
        print 'Processing file: %s' % fl
        try:
            r, z = qip2qutim(ipath + fl, opath)
            if r:
                rz += 1
                zz += z
            else:
                sk += 1
        except Exception as e:
            print e
            er += 1

    print '\nFiles processed: %d\nFiles writed: %d\nFiles skipped: %d\nErrors: %d\n' % (rz, zz, sk, er)

if __name__ == '__main__':
    main()
